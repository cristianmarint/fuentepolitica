+++
author = "AdriaanAlsema"
categories = ["Colombia"]
date = 2020-09-16T22:54:12Z
description = "El Ministerio de Salud de Colombia ha autorizado la reanudación paulatina de los vuelos internacionales que fueron cerrados en marzo debido a la pandemia de COVID-19, informaron este miércoles medios locales."
draft = false
image = "/uploads/bogotaheals-1170x585.jpg"
tags = ["noticia", "bogota", "vuelos","turismo","vieaje aereo"]
title = "Capital de Colombia inicia proceso de reconciliación para superar el trauma del terrorismo de Estado"
type = "featured"

+++


El Ministerio de Salud de Colombia ha autorizado la reanudación paulatina de los vuelos internacionales que fueron cerrados en marzo debido a la pandemia de COVID-19, informaron este miércoles medios locales.

Aún se desconoce cuándo y qué vuelos internacionales suspendidos el 23 de marzo se reanudarán, ya que la autoridad de aviación civil Aerocivil está negociando con las autoridades de aviación de otros países sobre las condiciones y restricciones.

Según el diario El Tiempo, la industria de la aviación pidió a Aerocivil que priorizara a Estados Unidos, Chile, Panamá, Ecuador y España que tradicionalmente han sido los principales países de origen.

El diario dijo que Perú también era una prioridad, pero que este país sudamericano mantendrá sus aeropuertos cerrados hasta diciembre.

Según El Tiempo, los aviones han estado vendiendo boletos después de que les dijeron que los aeropuertos reabrirían el 1 de septiembre, pero es posible que tengan que reprogramar estos vuelos ya que aún no se sabe cuándo se reanudarán los vuelos de manera efectiva.

Según otros medios locales, el Ministerio de Salud permitió a Aerocivil reanudar vuelos a países que tienen una tasa de contagio menor que Colombia, ya que “no persisten las condiciones para mantener cerrados los vuelos internacionales desde y hacia las grandes capitales del país”.

Sin embargo, en todos los casos, los vuelos entrantes deberán cumplir con las regulaciones de bioseguridad recomendadas por la Organización Mundial de la Salud e impuestas por el gobierno nacional.

> Si se cumplen las medidas de bioseguridad ya establecidas para vuelos internos, la probabilidad de que un viajero se infecte por COVID-19 es menor al 1% según el estudio de Barnett 2020.

##### Ministerio de salud

Además, la autoridad de aviación tendrá que seguir de cerca el comportamiento del coronavirus en los países de origen y reaccionar en consecuencia si las condiciones allí cambian.

Los vuelos nacionales se reanudarán el 1 de septiembre después de una serie de vuelos piloto para probar las medidas de bioseguridad tomadas en los aeropuertos del país.

El director de Aerocivil, Juan Carlos Salazar, dijo al diario El Espectador a principios de este mes que su agencia ha estado negociando con otros países sobre las condiciones específicas que permitirían vuelos hacia y desde Colombia.

Estas negociaciones en curso hacen que sea incierto cuándo aterrizará efectivamente el primer vuelo comercial internacional en uno de los aeropuertos internacionales de Colombia.

Todos los alcaldes de ciudades con aeropuertos internacionales han dado su permiso para reabrir sus aeropuertos.

Sin embargo, dijo Salazar, la normalización del tráfico aéreo podría demorar al menos un año.
