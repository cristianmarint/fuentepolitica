+++
author = " AdriaanAlsema"
categories = ["Colombia"]
date = 2020-09-16T22:34:56Z
description = "En un sorprendente giro en U, el jefe de policía interino de Colombia"
image = "/uploads/smad.jpg"
tags = ["noticia"]
title = "El dramático giro en U de Colombia para frenar la brutalidad policial sistemática"
type = "post"

+++

En un sorprendente giro en U, el jefe de policía interino de Colombia pidió el viernes perdón por la brutalidad policial, mientras que el gobierno del país prometió medidas para prevenir un estado autoritario.

El lamento expresado por el director interino de la Policía Nacional, General Gustavo Moreno, y las reformas específicas presentadas por el ministro de Defensa, Carlos Holmes Trujillo, se produjeron días después de que el brutal asesinato de un hombre desarmado por dos policías en Bogotá intensificara la violencia entre ciudadanos y policías.

Al igual que Trujillo más temprano el viernes, Moreno pidió explícitamente perdón por el asesinato del abogado de Bogotá Javier Ordoñez, de 46 años, el miércoles, pero no reconoció el asesinato de nueve personas y las lesiones de 75 personas que estaban siendo baleadas por la policía en una aparente campaña de terror.

El alcalde de Bogotá gana
Alcaldesa de Bogotá, Claudia López (Imagen: Twitter)
Las disculpas son una concesión importante a la alcaldesa de Bogotá, Claudia López, quien había exigido una disculpa tan pronto como se enteró de la muerte de Ordóñez la madrugada del miércoles.

Hasta el viernes, el autoritario Trujillo fue tan feroz como para condenar la violencia que había estallado y había herido a más de 190 policías mientras aparentemente consideraba legítima toda la violencia policial contra civiles.

López tomó la delantera el viernes cuando descubrió que la policía había desobedecido sus órdenes y el poderoso senador opositor Gustavo Petro acusó al presidente Iván Duque de usar a la policía para "dar un golpe" contra el alcalde de Bogotá y justificar la fuerza letal para sofocar cualquier protesta.

Para entonces, el inspector general Fernando Carrillo ya había abierto una investigación por el homicidio del abogado y anunció que investigaría también el aparente complot terrorista de la policía deshonesta.

Una vez que el presidente se vio acusado de terrorismo y de abusar de su poder para socavar a López, un crítico y el segundo funcionario público más poderoso de Colombia, Duque accedió con entusiasmo a reunirse con López.

Poco después, el ministro de Defensa llegó con una reforma policial específica que López también había exigido para frenar el abuso sistemático de poder por parte de la policía.

Fuente: https://colombiareports.com/colombias-dramatic-u-turn-to-curb-systematic-police-brutality/
