+++
author = "AdriaanAlsema"
categories = ["Colombia"]
date = 2020-09-16T22:54:12Z
description = "Bogotá, la capital de Colombia, inició el domingo un proceso de reconciliación para superar una semana traumática de terrorismo de Estado y violentas protestas contra la policía que dejaron 13 muertos y 403 heridos."
draft = false
image = "/uploads/bogotaheals-1170x585.jpg"
tags = ["noticia", "bogota", "duque"]
title = "Capital de Colombia inicia proceso de reconciliación para superar el trauma del terrorismo de Estado"
type = "featured"

+++



Bogotá, la capital de [Colombia,](https://colombiareports.com/bogota/ "Viajes a Bogotá") inició el domingo un proceso de reconciliación para superar una semana traumática de terrorismo de Estado y violentas protestas contra la policía que dejaron 13 muertos y 403 heridos.

La alcaldesa de Bogotá, [Claudia López,](https://colombiareports.com/claudia-lopez/) encabezó una ceremonia al aire libre en la céntrica Plaza [Bolívar](https://colombiareports.com/bolivar/ "Bolívar, Colombia") para restaurar la confianza entre los habitantes de la ciudad y la fuerza policial después de una explosión de violencia que la ciudad no había visto en décadas.

El brutal asesinato de un hombre desarmado por dos policías en la madrugada del miércoles desató protestas pacíficas y ataques a comisarías que el gobierno de extrema derecha del presidente [Iván Duque](https://colombiareports.com/ivan-duque/ "Ivan Duque |  Biografía") pretendía reprimir con extrema violencia policial.

----------

#### [El ministro de Defensa de Colombia deja en línea evidencia de orquestación del terrorismo](https://colombiareports.com/colombias-defense-minister-leaves-evidence-of-orchestrating-terrorism-online/)

----------

Desde entonces, 10 personas en la capital y tres en la vecina ciudad de Soacha fueron asesinadas a tiros por la policía que abrió fuego indiscriminadamente contra los manifestantes.

Los enfrentamientos entre vecinos y policías que aún se están reduciendo dejaron a 209 civiles y 194 policías heridos. Decenas de comisarías y puestos policiales quedaron reducidos a cenizas.

El presidente de extrema derecha no asistió a la ceremonia de paz y reconciliación organizada por el alcalde progresista, que había invitado a Duque.

La violencia ha destruido la legitimidad del gobierno y la confianza de los residentes de Bogotá en las autoridades, que la alcaldesa espera restaurar después de que ella recuperara el control de su fuerza policial el viernes.

La ceremonia inició con un discurso de Maira Paez, compañera de Jader Fonseca, uno de los civiles que fue asesinado por la policía durante la campaña de terrorismo de Estado.

> Quiero exigir que el gobierno nacional haga justicia, que la policía no mate ni hiera más gente, que todo el peso de la ley recaiga sobre ellos porque lo que hicieron fue un asesinato. Mi esposo no fue herido por una bala perdida, recibió cuatro disparos. Estaba acribillado. Han dejado a un niño sin derecho a conocer a su padre, mi hijo queda huérfano por este acto violento. El presidente debería estar aquí, la Policía Nacional pidiéndonos que lo perdonemos por lo sucedido.

##### Maira Paez

“Necesitamos que sean nuestros amigos y debemos ser sus amigos para hacer avanzar a nuestra nación en paz”, dijo Constanza Chaparro, cuyo familiar Brayan Rodríguez sobrevivió a la violencia.

Tanto líderes religiosos católicos como protestantes hablaron en un intento de curar las heridas de los residentes traumatizados cuyos líderes políticos se unieron para obligar al gobierno nacional a arrodillarse.

La capital planea continuar su resistencia contra Duque, quien fue elegido con el apoyo de los narcotraficantes, con una protesta pacífica y ruidosa a las 6 de la tarde.

En las próximas semanas, López planea participar activamente en eventos para restaurar la confianza entre los residentes y el departamento de policía local, mientras que los legisladores nacionales instan a una reforma policial de gran alcance.

Fuente: https://colombiareports.com/colombias-capital-begins-reconciliation-process-to-overcome-state-terrorism-trauma/
