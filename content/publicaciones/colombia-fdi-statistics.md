+++
author = "colombiareports"
categories = ["Colombia", "foreign-investment", "inversion-extranjera"]
date = 2020-09-16T23:12:14Z
description = "La inversión extranjera directa (IED) en Colombia creció como Topsy después de 2002, cuando la administración del ex presidente Álvaro Uribe (2002-2010) comenzó a promover activamente la inversión extranjera en el exterior."
image = "/uploads/dollars-1170x585.jpg"
tags = ["noticia", "dinero", "crecimiento"]
title = "Inversión extranjera directa total"
type = "post"

+++
La inversión extranjera directa (IED) en Colombia creció como Topsy después de 2002, cuando la administración del ex presidente Álvaro Uribe (2002-2010) comenzó a promover activamente la inversión extranjera en el exterior. Al parecer, las élites colombianas también comenzaron a repatriar capital que había estado oculto en paraísos fiscales extranjeros. Cuando el sucesor de Uribe, Juan Manuel Santos, comenzó a promover la inversión en otros sectores después de asumir el cargo en 2010, la IED en Colombia creció aún más.